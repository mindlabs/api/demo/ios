import Foundation
import Promises
import MindSDK

class MindAPI {

    private static let APPLICATION_URI = "https://api.mind.com/00e1db72-c57d-478c-88be-3533d43c8b34"
    private static let APPLICATION_TOKEN = "W1VJggwnvg2ldDdvSYES07tpLCWLDlD5nFakVJ6QSCPiZRpAMGyAzKW07OpM1IpceZ2WT5h5Mu7Ekt7WDTQzMUoQkVTRE4NdYUFE"

    private static let HTTP_CONNECTION_TIMEOUT_SECONDS: TimeInterval = 10

    static func createConference(_ name: String, _ layout: ConferenceLayout) -> Promise<String> {
        return newHttpPost(APPLICATION_URI, [ "name":  name, "layout": layout.rawValue, "features": [] ]).then({ responseDTO in
            let responseStatus = responseDTO["status"] as! Int
            if (responseStatus == 200) {
                let conferenceDTO = responseDTO["data"] as! [String: Any]
                return self.APPLICATION_URI + "/" + (conferenceDTO["id"] as! String)
            } else {
                throw NSError(domain: "Confy", code: 0, userInfo: [NSLocalizedDescriptionKey : "Can't create a room: \(responseStatus)"])
            }
        })
    }

    static func createParticipant(_ conferenceURI: String, _ name: String, _ layout: ConferenceLayout, _ role: ParticipantRole) -> Promise<String> {
        return newHttpPost(conferenceURI + "/participants", [ "name":  name, "layout": layout.rawValue, "role": role.rawValue ]).then({ responseDTO in
            let responseStatus = responseDTO["status"] as! Int
            switch (responseStatus) {
                case 200:
                    let participantDTO = responseDTO["data"] as! [String: Any]
                    return Promise(participantDTO["token"] as! String)
                case 403:
                    return newHttpGet(conferenceURI + "/participants").then({ responseDTO in
                        let responseStatus = responseDTO["status"] as! Int
                        if (responseStatus == 200) {
                            let participantDTOs = responseDTO["data"] as! [[String: Any]]
                            for i in 0..<participantDTOs.count {
                                let participantDTO = participantDTOs[i]
                                if !(participantDTO["online"] as! Bool) {
                                    return participantDTO as [String: Any]?
                                }
                            }
                            return nil as [String: Any]?
                        } else {
                            throw NSError(domain: "Confy", code: 0, userInfo: [NSLocalizedDescriptionKey : "Can't get the list of participants: \(responseStatus)"])
                        }
                    }).then({offlineParticipant in
                        if (offlineParticipant != nil) {
                            return newHttpDelete(conferenceURI + "/participants/" + (offlineParticipant!["id"] as! String)).then({ responseDTO in
                                let responseStatus = responseDTO["status"] as! Int
                                if (responseStatus == 204) {
                                    return MindAPI.createParticipant(conferenceURI, name, layout, role)
                                } else {
                                    throw NSError(domain: "Confy", code: 0, userInfo: [NSLocalizedDescriptionKey : "Can't free space in the room: \(responseStatus)"])
                                }
                            }) as Promise<String>
                        } else {
                            throw NSError(domain: "Confy", code: 0, userInfo: [NSLocalizedDescriptionKey : "The room is full"])
                        }
                    })
                case 404:
                    throw NSError(domain: "Confy", code: 0, userInfo: [NSLocalizedDescriptionKey : "The room doesn't exist"])
                default:
                    throw NSError(domain: "Confy", code: 0, userInfo: [NSLocalizedDescriptionKey : "Can't create a new participant: \(responseStatus)"])
            }
        }) as Promise<String>
    }

    private static func newHttpPost(_ uri: String, _ dto: [ String: Any ]) -> Promise<[String: Any]> {
        return MindAPI.newHttpRequest("post", uri, dto)
    }

    private static func newHttpGet(_ uri: String) -> Promise<[String: Any]> {
        return MindAPI.newHttpRequest("get", uri, nil)
    }

    private static func newHttpDelete(_ uri: String) -> Promise<[String: Any]> {
        return MindAPI.newHttpRequest("delete", uri, nil)
    }

    @discardableResult
    private static func newHttpRequest(_ method: String, _ url: String, _ dto: [ String: Any ]!) -> Promise<[String: Any]> {
        let result = Promise<[String: Any]>.pending()
        var request = URLRequest(url: URL(string: url)!, timeoutInterval: MindAPI.HTTP_CONNECTION_TIMEOUT_SECONDS)
        request.setValue("Bearer " + MindAPI.APPLICATION_TOKEN, forHTTPHeaderField: "Authorization")
        request.setValue("Mind iOS SDK \(MindSDK.VERSION)", forHTTPHeaderField: "Mind-SDK")
        request.setValue("\(UIDevice.current.systemName) \(UIDevice.current.systemVersion)", forHTTPHeaderField: "User-Agent")
        request.httpMethod = method
        request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
        if (dto != nil) {
            request.httpBody = try! JSONSerialization.data(withJSONObject: dto!, options: [])
        }
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            do {
                if let error = error {
                    if ((error as NSError).code != NSURLErrorCancelled) {
                        result.reject(error)
                    }
                } else {
                    let response = response as! HTTPURLResponse
                    let responseCode = response.statusCode
                    var responseDTO = [String: Any]()
                    responseDTO["status"] = responseCode
                    let content = String(bytes: data!, encoding: .utf8)!.trimmingCharacters(in: .whitespaces)
                    if content.count > 0 {
                        switch content.first {
                            case "[":
                                responseDTO["data"] = try JSONSerialization.jsonObject(with: content.data(using: .utf8)!, options: []) as! [Any]
                            case "{":
                                responseDTO["data"] = try JSONSerialization.jsonObject(with: content.data(using: .utf8)!, options: []) as! [String: Any]
                            default:
                                responseDTO["data"] = content
                        }
                    }
                    result.fulfill(responseDTO)
                }
            } catch {
                result.reject(error)
            }
        }
        task.resume()
        return result
    }

}
