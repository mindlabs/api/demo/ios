import UIKit
import AVFoundation
import MindSDK
import Promises

class RoomViewController: UIViewController, SessionListener {

    private static let PARTICIPANT_NAMES = [ "Zeus", "Hera", "Poseidon", "Ares", "Athena", "Apollo", "Artemis", "Hephaestus", "Aphrodite", "Hermes", "Dionysus", "Hades", "Hypnos", "Nike", "Janus", "Nemesis", "Iris", "Hecate", "Prometheus", "Cronus" ]

    var roomURI: String!

    private var camera: Camera!
    private var microphone: Microphone!
    private var session: Session!
    private var conference: Conference!
    private var me: Me!
    private var myStream: MediaStream!

    @IBOutlet var conferenceVideo: Video!

    @IBOutlet var curtain: UIView!
    @IBOutlet var curtainVerticalOffset: NSLayoutConstraint!
    @IBOutlet var curtainHorizontalOffset: NSLayoutConstraint!
    @IBOutlet var curtainHandle: UIView!

    @IBOutlet var toggleCameraButton: UIButton!
    @IBOutlet var toggleMicrophoneButton: UIButton!
    @IBOutlet var toggleSpeakerButton: UIButton!

    @IBOutlet var qrCode: UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()
        toggleCameraButton.isSelected = false
        toggleMicrophoneButton.isSelected = false
        toggleSpeakerButton.isSelected = true
        conferenceVideo.setScalingMode(.scaleAspectFill)
        camera = MindSDK.getCamera()
        microphone = MindSDK.getMicrophone()
        let conferenceURI: String = roomURI.replacingOccurrences(of: #"^(https?://[^/]+).*/(#/|\?)([^/]+)$"#, with: "$1/00e1db72-c57d-478c-88be-3533d43c8b34/$3", options: .regularExpression)
        qrCode.image = encodeTextIntoQRCodeImage(text: roomURI)
        MindAPI.createParticipant(conferenceURI, RoomViewController.PARTICIPANT_NAMES.randomElement()!, .MOSAIC, .MODERATOR).then({ token in
            return MindSDK.join2(conferenceURI, token, self).then({ session in
                self.session = session
                self.conference = session.getConference()
                self.me = self.conference.getMe()
                print("Joined conference \(self.conference.getId()) as participant \(self.me.getId())")
                let conferenceStream = self.conference.getMediaStream()
                conferenceStream.setMaxVideoBitrate(624_000) // 480 kbit/s (which corresponds to 640x360@30) + 30%
                self.conferenceVideo.setMediaStream(conferenceStream)
                self.myStream = MindSDK.createMediaStream(self.microphone, self.camera)
                self.me.setMediaStream(self.myStream)
                self.microphoneDidToggle(self.toggleMicrophoneButton)
                self.cameraDidToggle(self.toggleCameraButton)
            })
        }).catch({ error in
            self.alert(error.localizedDescription)
        })
        try? AVAudioSession.sharedInstance().overrideOutputAudioPort(.speaker)
        NotificationCenter.default.addObserver(forName: AVAudioSession.routeChangeNotification, object: nil, queue: nil, using: routeChange)
    }

    override func viewDidAppear(_ animated: Bool) {
        UIApplication.shared.isIdleTimerDisabled = true
    }

    override func viewDidDisappear(_ animated: Bool) {
        UIApplication.shared.isIdleTimerDisabled = false
    }

    @IBAction func cameraDidToggle(_ sender: UIButton) {
        if (!toggleCameraButton.isSelected) {
            toggleCameraButton.isEnabled = false
            camera.acquire().then({
                self.toggleCameraButton.isSelected = true
                self.toggleCameraButton.isEnabled = true
            }).catch({ error in
                self.toggleCameraButton.isEnabled = true
                if ((error as NSError).code == 0) {
                    AVCaptureDevice.requestAccess(for: .video) { granted in
                        if (granted) {
                            DispatchQueue.main.async {
                                self.cameraDidToggle(self.toggleCameraButton)
                            }
                        }
                    }
                }
            })
        } else {
            self.toggleCameraButton.isSelected = false
            camera.release()
        }
    }

    @IBAction func microphoneDidToggle(_ sender: UIButton) {
        if (!toggleMicrophoneButton.isSelected) {
            toggleMicrophoneButton.isEnabled = false
            microphone.acquire().then({
                self.toggleMicrophoneButton.isSelected = true
                self.toggleMicrophoneButton.isEnabled = true
            }).catch({ error in
                self.toggleMicrophoneButton.isEnabled = true
                if ((error as NSError).code == 0) {
                    AVCaptureDevice.requestAccess(for: .audio) { granted in
                        if (granted) {
                            DispatchQueue.main.async {
                                self.microphoneDidToggle(self.toggleMicrophoneButton)
                            }
                        }
                    }
                }
            })
        } else {
            self.toggleMicrophoneButton.isSelected = false
            microphone.release()
        }
    }

    @IBAction func speakerDidToggle(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        conferenceVideo.setVolume(toggleSpeakerButton.isSelected ? 1.0 : 0.0)
    }

    @IBAction func closeDidToggle(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }

    @IBAction func didTapConferenceVideo(_ recognizer: UITapGestureRecognizer) {
        let minVerticalOffset = curtainHandle.frame.height - curtain.frame.height
        let minHorizontalOffset = curtainHandle.frame.width - curtain.frame.width
        UIView.animate(withDuration: 0.3, animations: {
            if (self.curtainVerticalOffset.isActive) {
                self.curtainVerticalOffset.constant = self.curtainVerticalOffset.constant == minVerticalOffset ? -self.curtain.frame.height : minVerticalOffset
                self.curtainHorizontalOffset.constant = self.curtainVerticalOffset.constant
            }
            if (self.curtainHorizontalOffset.isActive) {
                self.curtainHorizontalOffset.constant = self.curtainHorizontalOffset.constant == minHorizontalOffset ? -self.curtain.frame.width : minHorizontalOffset
                self.curtainVerticalOffset.constant = self.curtainHorizontalOffset.constant
            }
            self.view.layoutIfNeeded()
        })
    }

    @IBAction func didPanCurtain(_ recognizer: UIPanGestureRecognizer) {
        let minVerticalOffset = curtainHandle.frame.height - curtain.frame.height
        let minHorizontalOffset = curtainHandle.frame.width - curtain.frame.width
        switch recognizer.state {
            case .changed:
                let newVerticalOffset = curtainVerticalOffset.constant - recognizer.translation(in: curtain).y;
                curtainVerticalOffset.constant = min(max(newVerticalOffset, minVerticalOffset), 0)
                let newHorizontalOffset = curtainHorizontalOffset.constant - recognizer.translation(in: curtain).x;
                curtainHorizontalOffset.constant = min(max(newHorizontalOffset, minHorizontalOffset), 0)
                recognizer.setTranslation(CGPoint.zero, in: curtain)
            case .ended:
                UIView.animate(withDuration: 0.3, animations: {
                    if (self.curtainVerticalOffset.isActive) {
                        self.curtainVerticalOffset.constant = recognizer.velocity(in: self.curtain).y < 0 ? 0 : minVerticalOffset
                        self.curtainHorizontalOffset.constant = self.curtainVerticalOffset.constant
                    }
                    if (self.curtainHorizontalOffset.isActive) {
                        self.curtainHorizontalOffset.constant = recognizer.velocity(in: self.curtain).x < 0 ? 0 : minHorizontalOffset
                        self.curtainVerticalOffset.constant = self.curtainHorizontalOffset.constant
                    }
                    self.view.layoutIfNeeded()
                })
            default:
                break;
        }
    }

    func onMeExpelled(_ me: Me) {
        dismiss(animated: true, completion: nil)
    }

    func onConferenceEnded(_ conference: Conference) {
        dismiss(animated: true, completion: nil)
    }

    private func alert(_ message: String) {
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: { _ in
            self.dismiss(animated: true, completion: nil)
        }))
        self.present(alert, animated: true, completion: nil)
    }

    private func encodeTextIntoQRCodeImage(text: String) -> UIImage? {
        let qrFilter = CIFilter(name: "CIQRCodeGenerator")
        qrFilter?.setValue(text.data(using: String.Encoding.ascii), forKey: "inputMessage")
        let scaledQrImage = qrFilter?.outputImage?.transformed(by: CGAffineTransform(scaleX: 5, y: 5))
        let colorInvertFilter = CIFilter(name: "CIColorInvert")
        colorInvertFilter?.setValue(scaledQrImage, forKey: "inputImage")
        let maskToAlphaFilter = CIFilter(name: "CIMaskToAlpha")
        maskToAlphaFilter?.setValue(colorInvertFilter?.outputImage, forKey: "inputImage")
        let context = CIContext()
        let cgImage = context.createCGImage(maskToAlphaFilter!.outputImage!, from: maskToAlphaFilter!.outputImage!.extent)
        return UIImage(cgImage: cgImage!)
    }

    override func dismiss(animated flag: Bool, completion: (() -> Void)? = nil) {
        conferenceVideo.setMediaStream(nil)
        if (session != nil) {
            MindSDK.exit2(session)
            session = nil
        }
        microphone.release()
        camera.release()
        super.dismiss(animated: flag, completion: completion)
    }

    private func routeChange(notification: Notification) {
        if let info = notification.userInfo, let value = info[AVAudioSessionRouteChangeReasonKey] as? UInt, let reason = AVAudioSession.RouteChangeReason(rawValue: value) {
            if (reason == .categoryChange) {
                try? AVAudioSession.sharedInstance().overrideOutputAudioPort(.speaker)
            }
        }
    }

}
