import UIKit
import AVFoundation
import Promises
import MindSDK

class MainViewController: UIViewController, AVCaptureMetadataOutputObjectsDelegate {

    private static let CONFERENCE_NAMES = [ "Cassiopeia", "Andromeda", "Taurus", "Scorpius", "Auriga", "Orion", "Hydrus", "Centaurus", "Draco", "Lyra" ]

    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }

    @IBOutlet var cameraPreview: UIView!

    private var cameraPreviewLayer: AVCaptureVideoPreviewLayer!
    private var captureSession: AVCaptureSession!

    private var roomURI: String!

    override func viewDidLoad() {
        super.viewDidLoad()
        cameraPreviewLayer = AVCaptureVideoPreviewLayer()
        cameraPreviewLayer.videoGravity = .resizeAspectFill
        cameraPreviewLayer.connection?.videoOrientation = .portrait
        cameraPreviewLayer.frame = view.bounds
        cameraPreview.layer.addSublayer(cameraPreviewLayer)
    }

    override func viewWillAppear(_ animated: Bool) {
        roomURI = nil
        switch AVCaptureDevice.authorizationStatus(for: .video) {
            case .authorized:
                self.cameraAccessDidGrant()
            case .notDetermined:
                AVCaptureDevice.requestAccess(for: .video) { granted in
                    if (granted) {
                        self.cameraAccessDidGrant()
                    }
                }
            default:
                break
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        if (captureSession != nil) {
            captureSession.stopRunning()
            captureSession = nil
        }
    }

    private func cameraAccessDidGrant() {
        if let videoCaptureDevice = AVCaptureDevice.default(for: .video) {
            captureSession = AVCaptureSession()
            let videoInput: AVCaptureDeviceInput = try! AVCaptureDeviceInput(device: videoCaptureDevice)
            captureSession.addInput(videoInput)
            let metadataOutput = AVCaptureMetadataOutput()
            captureSession.addOutput(metadataOutput)
            metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            metadataOutput.metadataObjectTypes = [ .qr ]
            cameraPreviewLayer.session = captureSession
            DispatchQueue.global(qos: .background).async {
                self.captureSession.startRunning()
            }
        }
    }

    func roomURIDidDetect(_ roomURI: String) {
        if (self.roomURI == nil) {
            self.roomURI = roomURI
            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            performSegue(withIdentifier: "goToRoom", sender: nil)
        }
    }
    
    @IBAction func createRoomButtonDidPress(_ sender: UIButton) {
        if (self.roomURI == nil) {
            self.roomURI = "pending"
            MindAPI.createConference(MainViewController.CONFERENCE_NAMES.randomElement()!, .MOSAIC).then({ conferenceURI in
                self.roomURI = conferenceURI.replacingOccurrences(of: "/00e1db72-c57d-478c-88be-3533d43c8b34/", with: "/demo/#/")
                self.performSegue(withIdentifier: "goToRoom", sender: nil)
            })
        }
    }

    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        if let qrCode = (metadataObjects.first as? AVMetadataMachineReadableCodeObject) {
            if (qrCode.stringValue?.range(of: #"^https?://[^/]+.*/(#/|\?)[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$"#, options: .regularExpression) != nil) {
                roomURIDidDetect(qrCode.stringValue!)
            }
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let roomViewController = segue.destination as? RoomViewController {
            roomViewController.roomURI = roomURI
        }
    }

}
